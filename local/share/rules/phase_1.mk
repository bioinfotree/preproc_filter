### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Apr 10 18:01:04 2013 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

INPUT_FASTA ?=
SPECIE_DB = $(addsuffix _EST, $(SPECIE))
SPECIE_FLAG = $(addsuffix _EST.flag, $(SPECIE))
LAMPREY_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/petromyzon_marinus/cdna/Petromyzon_marinus.Pmarinus_7.0.66.cdna.all.fa.gz
README = ftp://ftp.ensembl.org/pub/release-66/fasta/petromyzon_marinus/cdna/README



# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@

%_EST.flag:
	$(call download_DB, $(LAMPREY_EST), $(README), $@)

SPECIE_XML_GZ = $(addsuffix _EST.xml.gz, $(SPECIE))
%_EST.xml.gz: query.fasta %_EST.flag
	!threads
	$(call search_DB, $<, ./$(basename $^2)/$(basename $^2), $@)

SPECIE_TAB = $(addsuffix _EST.tab, $(SPECIE))
%_EST.tab: %_EST.xml.gz
	$(call parse_result, $<, $@)



define download_DB
	mkdir -p $(basename $3) && \
	cd $(basename $3) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	wget -q -O $(basename $3).gz $1 && \   * cDNA sequence downloaded from ensembl release 66 *
	wget -q  $2 && \
	gunzip $(basename $3).gz && \
	makeblastdb -in $(basename $3) -dbtype nucl -title $(basename $3) && \
	cd .. && \
	touch $3
endef


define search_DB
	tblastx -evalue 0.001 -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $1 -db $2 -out $(basename $3); \
	if gzip -cv $(basename $3) > $3; \
	then \
	rm $(basename $3); \
	fi
endef


define parse_result
	zcat -dc $1 | blast_fields -f -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand -e $(EVAL) > $2
endef






ALL +=

INTERMEDIATE +=

CLEAN +=

######################################################################
### phase_1.mk ends here
